# Recipe

## Reference:
* https://docs.microsoft.com/en-us/aspnet/core/security/docker-https?view=aspnetcore-5.0

## Snippet
```powershell
dotnet dev-certs https -ep $env:USERPROFILE\.aspnet\https\WebApi.pfx -p crypticpassword
dotnet dev-certs https --trust
dotnet user-secrets -p .\src\WebApi\WebApi.csproj set "Kestrel:Certificates:Development:Password" "crypticpassword"
```